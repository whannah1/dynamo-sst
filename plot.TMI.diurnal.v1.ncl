load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
begin

	lat1 =  0.
	lat2 =  7.
	lon1 =  70.
	lon2 =  80.
	
	lat1 =  -10.
	lat2 =   10.
	lon1 =  60.
	lon2 =  90.
	
	fig_type = "png"
	fig_file = "~/Research/DYNAMO/SAT/TMI/TMI.diurnal.v1.IO"

;======================================================================================
;======================================================================================
  ifile = "~/Data/DYNAMO/SAT/TMI/TMI.SST.2011-11.3hr.nc"
  infile = addfile(ifile,"r")
  SST = infile->SST(:,{lat1:lat2},{lon1:lon2})
  SST = (/SST-273./)
  SST@units = "deg C"
  ;aSST = dim_avg_n_Wrap(SST,(/1,2/))
  aSST = dim_avg_n(SST(:,{lat1:lat2},{lon1:lon2}),(/1,2/))
  time = infile->time
  time@units = "day since 2011-11-01 00:00:00"

	aSST!0 = "time"
	aSST&time = time
  
  nhr = 8
  dhr = 24/nhr
  hour = new(nhr+1,float)
  hour = ispan(0,24,dhr);%24
  dcycle = new(nhr+1,float)
  do h = 0,nhr-1
    dcycle(h) = avg(aSST(h::nhr))
  end do
  dcycle(h) = dcycle(0)
;======================================================================================
;======================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(3,graphic)
  	res = True
  	res@gsnFrame 	= False
  	res@gsnDraw 		= False
  	res@vpHeightF	= 0.3
  	res@tmXTOn 		= False
  	
  	res0 = res
  	res1 = res
  	mres = res
  	
  	res0@gsnLeftString = "Domain Avg SST"
  	res1@gsnLeftString = "Domain Avg Diurnal Cycle of SST"
  	mres@gsnLeftString = "Time Avg SST"
  	
  	res0@trXMaxF = max(time)
  	res0@tmXBLabelAngleF = -45.
  	res0@tmXBLabelFontHeightF = 0.01
  	
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat = "%D %c"
  	tres@ttmAxis = "XB"
  	tres@ttmMajorStride = 2*8
  	tres@ttmMinorStride = 1*8
  	time_axis_labels(time,res0,tres)
  	
  plot(0) = gsn_csm_xy(wks,time,aSST,res0)
  plot(1) = gsn_csm_xy(wks,hour,dcycle,res1)
  		
  	mres@cnFillOn = True
    	mres@cnLinesOn = False
  	mres@gsnSpreadColors = True
    	mres@mpLimitMode 	= "LatLon"
    	mres@mpMinLatF		= -15
    	mres@mpMaxLatF		=  15
    	mres@mpMinLonF		=  50
    	mres@mpMaxLonF		= 110
    	mres@gsnAddCyclic	= False
  plot(2) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(SST,0),mres)
;======================================================================================
;======================================================================================
	pres = True
	pres@txString = lat1+":"+lat2+" N"+"   "+lon1+":"+lon2+" E"
	pres@gsnPanelBottom = 0.05
	  
  gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
  
	print("")
	print("  "+fig_file+"."+fig_type)
	print("")
	
;======================================================================================
;======================================================================================
end
